#!/bin/sh

# TODO's:
# * find free port for communication for nginx and uwsgi
# * test conjob creation properly

# port between nginx and uwsgi
nginx_uwsgi_port=23761
webapp_port=24428
webapp_name="web2py_stable"
NGINX_VERSION="1.2.3" # stable 2012-08-07
UWSGI_VERSION="latest" # latest stable


echo 'Install script for nginx, uwsgi  and web2py (latest stable)'
echo 'If you wish to create cronjobs comment out the last lines of this script'

# Get web2py admin password
echo "Web2py admin password:"
read  web2py_password

# Get webfaction application name
echo "Webfaction application name:"
#read  webapp_name
echo $webapp_name

# Get webfaction port number
echo "Webfaction application port:"
#read  webapp_port
echo $webapp_port

# port betweet nginx and uwsgi
echo "Port number for communication between nginx and uswgi (eg. 9001):"
#read nginx_uwsgi_port
echo $nginx_uwsgi_port


### 
### web2py
###
mkdir -p ~/webapps/${webapp_name}
cd ~/webapps/${webapp_name}
wget -c http://www.web2py.com/examples/static/web2py_src.zip
unzip web2py_src.zip
rm web2py_src.zip
cd web2py
python2 -c "from gluon.main import save_password; save_password('${web2py_password}',443)"
cd ..

###
### nginx
###
mkdir downs
cd downs

# download and install nginx in the appname directory

wget http://nginx.org/download/nginx-$NGINX_VERSION.tar.gz
tar xvf nginx-$NGINX_VERSION.tar.gz
cd nginx-$NGINX_VERSION

./configure \
  --prefix=/home/${USER}/webapps/${webapp_name}/nginx \
  --sbin-path=/home/${USER}/webapps/${webapp_name}/nginx/sbin/nginx \
  --conf-path=/home/${USER}/webapps/${webapp_name}/nginx/nginx.conf \
  --error-log-path=/home/${USER}/webapps/${webapp_name}/nginx/log/error.log \
  --pid-path=/home/${USER}/webapps/${webapp_name}/nginx/run/nginx.pid  \
  --lock-path=/home/${USER}/webapps/${webapp_name}/nginx/lock/nginx.lock \
  --with-http_gzip_static_module \
  --http-log-path=/home/${USER}/webapps/${webapp_name}/nginx/log/access.log \
  --http-client-body-temp-path=/home/${USER}/webapps/${webapp_name}/nginx/tmp/client/ \
  --http-proxy-temp-path=/home/${USER}/webapps/${webapp_name}/nginx/tmp/proxy/ \
  --http-fastcgi-temp-path=/home/${USER}/webapps/${webapp_name}/nginx/tmp/fcgi/
make && make install
cd ..

# configure nginx
cd ..
mkdir nginx/tmp
mkdir nginx/tmp/client
cd nginx

echo "
worker_processes  2;

events {
    worker_connections  1024;
}

http {
    access_log  /home/${USER}/logs/user/access_appname.log  combined;
    error_log   /home/${USER}/logs/user/error_appname.log   crit;

    include                mime.types;
    client_max_body_size   5m;
    default_type           application/octet-stream;
    gzip_static            on;
    gzip_vary              on;
    sendfile               on;
    tcp_nodelay            on;

    server {
        listen ${webapp_port};
        location ~* /(\w+)/static/ {
            root /home/${USER}/webapps/${webapp_name}/web2py/applications/;
        }
        location / {
            uwsgi_pass      127.0.0.1:${nginx_uwsgi_port};
            include         uwsgi_params;
        }
    }
}
" > nginx.conf

echo "#!/bin/sh
kill -s QUIT \$( cat /home/${USER}/webapps/${webapp_name}/nginx/run/nginx.pid )
" > stop
chmod +x stop

# start nginx
./sbin/nginx -c /home/${USER}/webapps/${webapp_name}/nginx/nginx.conf
cd ..

### 
### uwsgi
###
mkdir uwsgi
cd downs

# download and install uwsgi in the appname directory

wget http://projects.unbit.it/downloads/uwsgi-$UWSGI_VERSION.tar.gz
tar xvf uwsgi-$UWSGI_VERSION.tar.gz

#cd uwsgi-$UWSGI_VERSION
cd uwsgi-*
#make -f Makefile.Py26
python2.7 uwsgiconfig.py --build
cp uwsgi ../../uwsgi/

cd ../../uwsgi
echo "<uwsgi>
    <socket>127.0.0.1:${nginx_uwsgi_port}</socket>
    <workers>2</workers>
    <no-orphans/>
    <pythonpath>/home/${USER}/webapps/${webapp_name}/web2py</pythonpath>
    <pidfile>/home/${USER}/webapps/${webapp_name}/uwsgi/uwsgi.pid</pidfile>
    <daemonize>/home/${USER}/webapps/${webapp_name}/uwsgi/uwsgi.log</daemonize>
    <module>wsgihandler</module>
</uwsgi>" > uwsgi.xml

echo "#!/bin/sh
kill -s QUIT \$( cat /home/${USER}/webapps/${webapp_name}/uwsgi/uwsgi.pid )
" > stop
chmod +x stop


# start uwsgi
./uwsgi -x /home/${USER}/webapps/${webapp_name}/uwsgi/uwsgi.xml
cd ..

###
### cleanup
###
rm -rf downs

##
## create start/stop scripts
##
echo "#!/bin/sh
/home/${USER}/webapps/${webapp_name}/nginx/sbin/nginx -c /home/${USER}/webapps/${webapp_name}/nginx/nginx.conf
/home/${USER}/webapps/${webapp_name}/uwsgi/uwsgi -x /home/${USER}/webapps/${webapp_name}/uwsgi/uwsgi.xml
" > start
chmod +x start

echo "#!/bin/sh
kill -SIGTERM \$( cat /home/${USER}/webapps/${webapp_name}/uwsgi/uwsgi.pid )
kill -SIGTERM \$( cat /home/${USER}/webapps/${webapp_name}/nginx/run/nginx.pid )
" > stop
chmod +x stop

###
### create cronjobs
###

#nginx_cron="10,30,50 * * * * /home/${USER}/webapps/${webapp_name}/nginx/sbin/nginx -c /home/${USER}/webapps/${webapp_name}/nginx/nginx.conf"
#(crontab -l; echo "$nginx_cron" ) | crontab -
# 
#uwsgi_cron="10,30,50 * * * * /home/${USER}/webapps/${webapp_name}/uwsgi/uwsgi -x /home/${USER}/webapps/${webapp_name}/uwsgi/uwsgi.xml"
#(crontab -l; echo "$uwsgi_cron" ) | crontab -
